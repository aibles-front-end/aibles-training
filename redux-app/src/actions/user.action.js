import * as actionTypes from "../types/user.type";

export const fetchDataRequest = () => ({
  type: actionTypes.FETCH_DATA_REQUEST,
});

export const fetchDataSuccess = (data) => ({
  type: actionTypes.FETCH_DATA_SUCCESS,
  payload: data,
});

export const fetchDataFailure = (error) => ({
  type: actionTypes.FETCH_DATA_FAILURE,
  payload: error,
});
