import * as actionTypes from "../types/money.types"

export const onDeposit = money => ({
    type: actionTypes.DEPOSIT,
    payload: money
})

export const onWithdraw = money => ({
    type: actionTypes.WITHDRAW,
    payload: money
})