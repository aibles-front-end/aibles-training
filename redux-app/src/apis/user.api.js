import axios from "axios";

import {
  fetchDataRequest,
  fetchDataSuccess,
  fetchDataFailure,
} from "../actions/user.action";
import store from "../store";

export const fetchData = () => {
  store.dispatch(fetchDataRequest());
  return axios
    .get("https://jsonplaceholder.typicode.com/todos")
    .then((response) => {
      store.dispatch(fetchDataSuccess(response.data));
    })
    .catch((error) => {
      store.dispatch(fetchDataFailure(error.message));
    });
};
