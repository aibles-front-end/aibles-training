import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { onDeposit, onWithdraw } from "./actions/money.action";
import { fetchData } from "./thunks/user.thunk";
import axios from "axios";
import {
  fetchDataRequest,
  fetchDataSuccess,
  fetchDataFailure,
} from "./actions/user.action";
import "./App.css";

function App() {
  const [money, setMoney] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    // dispatch(fetchData());

    dispatch(fetchDataRequest());
    return axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then((response) => {
        dispatch(fetchDataSuccess(response.data));
      })
      .catch((error) => {
        dispatch(fetchDataFailure(error.message));
      });
  }, []);

  const { data } = useSelector((state) => state.user);

  const { totalMoney, errorMessage, profile } = useSelector(
    (state) => state.money
  );

  const handleDeposit = () => {
    dispatch(onDeposit(money));
    setMoney(0);
  };

  const handleWithdraw = () => dispatch(onWithdraw(money));

  return (
    <div className="App">
      {/* <h1>Bank</h1>
      <h2>Your money</h2>
      <p>{totalMoney} USD</p>
      <input
        type="number"
        required
        value={money}
        onChange={(e) => setMoney(parseInt(e.target.value))}
      />
      {errorMessage && <span>{errorMessage}</span>}
      <div>
        <button onClick={handleDeposit}>Deposit</button>
        <button onClick={handleWithdraw}>Withdraw</button>
      </div> */}
      <h1>Data</h1>
      {data.map((item, index) => (
        <p key={index}>{item.title}</p>
      ))}
    </div>
  );
}

export default App;
