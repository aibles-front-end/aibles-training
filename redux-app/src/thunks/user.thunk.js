import axios from "axios";

import {
  fetchDataRequest,
  fetchDataSuccess,
  fetchDataFailure,
} from "../actions/user.action";

export const fetchData = () => (dispatch) => {
  dispatch(fetchDataRequest());

  return axios
    .get("https://jsonplaceholder.typicode.com/todos")
    .then((response) => {
      dispatch(fetchDataSuccess(response.data));
    })
    .catch((error) => {
      dispatch(fetchDataFailure(error.message));
    });
};
