import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";

// const myMiddleware = (store) => (next) => (action) => {
//   return next(action);
// };

const store = createStore(
  rootReducer,
  // use Redux dev tool
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

//  can thiệp vào trong Redux để tiền xử lý, đứng giữa action và reducer
// const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
