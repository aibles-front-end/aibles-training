import { combineReducers } from "redux";
import money from "./money.reducer";
// import money from "./moneyImmer.reducer";
import user from "./user.reducer";

export default combineReducers({
  money,
  user,
});
