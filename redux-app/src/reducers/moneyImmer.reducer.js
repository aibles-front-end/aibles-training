import produce from "immer";
import * as actionTypes from "../types/money.types";

const initialState = {
  totalMoney: 0,
  errorMessage: "",
};

//  thay thế cho việc sử dụng  spread operator 
const moneyReducer = produce((draft, action) => {
  switch (action.type) {
    case actionTypes.DEPOSIT:
      draft.totalMoney += action.payload;
      draft.errorMessage = "";
      break;

    case actionTypes.WITHDRAW:
      const newMoney = draft.totalMoney - action.payload;
      if (newMoney < 0) {
        draft.errorMessage = "Ban khong co du tien";
      } else {
        draft.totalMoney = newMoney;
        draft.errorMessage = "";
      }
      break;

    default:
      break;
  }
}, initialState);

export default moneyReducer;