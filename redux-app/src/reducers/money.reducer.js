import * as actionTypes from "../types/money.types";

const initialState = {
  totalMoney: 0,
  errorMessage: "",
  profile: {
    name: "Thuc",
  },
};

const moneyReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.DEPOSIT:
      //   nên
      return {
        ...state,
        totalMoney: state.totalMoney + action.payload,
        errorMessage: "",
      };

    //  không nên
    // state.totalMoney = state.totalMoney + action.payload;
    // return state;
    case actionTypes.WITHDRAW:
      const newMoney = state.totalMoney - action.payload;
      if (newMoney < 0) {
        return { ...state, errorMessage: "Ban khong co du tien" };
      }

      return { ...state, totalMoney: newMoney };
    default:
      return state;
  }
};

export default moneyReducer;
