import React, { useEffect, useState } from "react";
import Children from "./Children";

export default function Parent() {
  const [counter, setCounter] = useState(0);
  const [isShowChildren, setIsShowChildren] = useState(true);
  const [currentSecond, setCurrentSecond] = useState(0);

  useEffect(() => {
    // Only execute after the first RENDER or count state changes
    console.log("useEffect has been called!");
  }, [counter]);

  const changeCount = () => {
    setCounter(counter + 1);
  };

  const changeShowChildren = () => {
    setIsShowChildren(!isShowChildren);
  };

  const changeCurrentSecond = () => {
    setCurrentSecond(currentSecond + 1);
  };

  return (
    <div>
      <br />
      <br />
      <button onClick={() => changeShowChildren()}>hidden Children</button>
      <h2>you click {setCounter} lần</h2>

      {isShowChildren && (
        <Children
          counter={counter}
          currentSecond={currentSecond}
          handleChangeCount={changeCount}
          handleChangeCurrentSecond={changeCurrentSecond}
        />
      )}
    </div>
  );
}
