import React, { useEffect } from "react";

export default function Children({
  counter,
  handleChangeCount,
  currentSecond,
  handleChangeCurrentSecond,
}) {
  useEffect(() => {
    const timer = setInterval(() => {
      handleChangeCurrentSecond();
    }, 1000);

    return () => {
      // Execute before the next effect or unmount.
      clearTimeout(timer);
    };
  });

  return (
    <div>
      <p>{counter}</p>
      <button onClick={handleChangeCount}>changeCount</button>

      <p>====== count Time</p>
      <p>time: {currentSecond}</p>
    </div>
  );
}
