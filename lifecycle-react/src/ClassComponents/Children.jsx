import React, { Component } from "react";

export default class Children extends Component {
  constructor(props) {
    super(props);
  }

  // componentDidMount() {
  //   this.timer = setInterval(() => {
  //     this.props.handleChangeCurrentSecond();
  //   }, 1000);
  // }

  // shouldComponentUpdate(){
  //   return false
  // }

  componentWillReceiveProps() {
    console.log("componentWillReceiveProps is running");
  }

  componentWillUpdate() {
    console.log("componentWillUpdate is running");
  }

  componentDidUpdate() {
    console.log("componentDidUpdate is running");
  }

  // componentWillUnmount() {
  //   if (this.timer) {
  //     clearInterval(this.timer);
  //   }
  // }

  // componentWillReceiveProps() và componentWillUpdate() không còn được sử dụng từ bản React 16
  // => dùng componentDidUpdate()
  render() {
    // console.log("render is running from children");
    return (
      <div>
        <p>{this.props.counter}</p>
        <button onClick={this.props.handleChangeCount}>changeCount</button>

        {/* <p>====== count Time</p>
        <p>time: {this.props.currentSecond}</p> */}
      </div>
    );
  }
}
