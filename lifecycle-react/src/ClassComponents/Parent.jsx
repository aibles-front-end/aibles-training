import React, { Component } from "react";
import Children from "./Children";

export default class Parent extends Component {
  // =============>   Mounting
  constructor(props) {
    super(props);
    this.state = { counter: 0, isShowChildren: true, currentSecond: 0 };
    console.log("constructor is running");
  }

  // componentWillMount() {
  //   console.log("componentWillMount is running");
  // }

  componentDidMount() {
    console.log("componentDidMount is running");
    // call api here
  }

  // componentWillMount() không còn được sử dụng từ bản React 16
  // => dùng componentDidMount()

  changeCount = () => {
    this.setState({ ...this.state, counter: this.state.counter + 1 });
  };

  changeShowChildren = () => {
    this.setState({
      ...this.state,
      isShowChildren: !this.state.isShowChildren,
    });
  };

  changeCurrentSecond = () => {
    console.log("currentSecond: ", this.state.currentSecond);
    this.setState({
      ...this.state,
      currentSecond: this.state.currentSecond + 1,
    });
  };

  render() {
    // console.log("render is running");

    if (this.state.counter === 5) {
      // Simulate a JS error
      throw new Error("I crashed!");
    }
    return (
      <div>
        <br />
        <br />
        <button onClick={() => this.changeShowChildren()}>
          hidden Children
        </button>
        <br />

        <p>you click {this.state.count}</p>
        {this.state.isShowChildren && (
          <Children
            counter={this.state.counter}
            currentSecond={this.state.currentSecond}
            handleChangeCount={this.changeCount}
            handleChangeCurrentSecond={this.changeCurrentSecond}
          />
        )}
      </div>
    );
  }
}
