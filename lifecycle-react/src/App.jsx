import logo from "./logo.svg";
import "./App.css";
import DemoClassComponents from "./ClassComponents/Parent";
import ErrorBoundary from "./ClassComponents/ErrorBoundary";
import DemoFunctionComponents from "./FunctionComponent/Parent";

function App() {
  return (
    <div className="App">
      <ErrorBoundary>
        {/* <DemoClassComponents /> */}
        <DemoFunctionComponents />
      </ErrorBoundary>
    </div>
  );
}

export default App;
