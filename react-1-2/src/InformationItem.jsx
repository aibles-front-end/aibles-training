
const InformationItem = (props) => {
  const {title,content}=props
  return (
    <div>
      <strong>{title} :</strong> <span>{content}</span>
    </div>
  );
}

export default InformationItem


