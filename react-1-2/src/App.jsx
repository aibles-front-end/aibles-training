import { useState, useEffect } from "react";
import "./App.css";
import Img from "./Img";
import InformationItem from "./InformationItem.jsx";

export default function App() {
  const age = 20;
  const [count, setCount] = useState(0);
  let countCopy = 0;
  const [name, setName] = useState("Le Trung Thuc");

  useEffect(() => {
    console.log("get danh sach sinh vien");
    return () => {
      console.log("vao day", count);
    };
  }, []);

  const introduce = (age) => (
    <div>
      <p>Họ tên: Lê Trung Thực</p>
      <p>Quê quán: Phú Xuyên, Hà Nội</p>
      <p>Tuổi: {age}</p>
    </div>
  );

  const handleCount = () => {
    setCount(count + 1);
    countCopy = countCopy + 1;
  };

  const informationArr = [
    { title: "Ho ten", content: "Le Trung Thuc", id: 0 },
    { title: "Que quan", content: "Phu Xuyen Ha Noi", id: 1 },
    { title: "Tuoi", content: 24, id: 2 },
  ];

  return (
    <div className="App">
      <p>Chao buổi tối</p>
      {/* <div>{introduce(20)}</div> */}
      {/* <p>{age}</p> */}
      {/* <Img/> */}
      <button onClick={handleCount}>tang count</button>
      <input
        type="text"
        onChange={(event) => console.log("event", event.target.value)}
      />
      <p>Count:{count}</p>
      <p>countCopy:{countCopy}</p>

      {/* <InformationItem title="Ho ten" content="Le Trung Thuc" />
      <InformationItem title="Que quan" content="Phu Xuyen Ha Noi" />
      <InformationItem title="Tuoi" content={age} /> */}

      {/* {informationArr.map((item) => {
        return <InformationItem title={item.title} content={item.content} />;
      })} */}

      {informationArr.map((item, index) => (
        <InformationItem
          key={item.id}
          title={item.title}
          content={item.content}
        />
      ))}
    </div>
  );
}

// export default App;
