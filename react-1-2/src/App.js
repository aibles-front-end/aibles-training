import logo from "./logo.svg";
import "./App.css";

export default function App() {
  const a = 20;
  const b = (
    <div>
      <p>Họ tên: Lê Trung Thực</p>
      <p>Quê quán: Phú Xuyên, Hà Nội</p>
    </div>
  );

  return (
    <div className="App">
      <p>Chao buổi tối</p>
      {b}
    </div>
  );
}

// export default App;
