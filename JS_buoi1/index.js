// 1
// console.log("Hello world")

// Variable
var name;
var _name;
var name1;
var fullName;
var full_name;
var $name;
// var 1name;

// Primitive types
// var number = 8; // Number
// var boolean = true; // Boolean
// var string = "hello kitty"; // String
// var x = null;
// var y;
// console.log(typeof x)

// Object types
// var object = new Object()
// var obj = {}
// var arr = []
// function func() {
// }
// console.log(typeof func)

// Operators
// variables = condition ? value1: value2;
// var a = 10
// var b = (a < 9) ? 5 : 4
// console.log(b)

// Conditions
// var hour = 17
// var greeting;

// if (hour < 18) {
//     greeting = "Good day";
// } else {
//     greeting = "Good evening";
// }

// var day = 6;
// var text;
// switch (day) {
//     case 6:
//         text = "Today is Saturday";
//         break;
//     case 0:
//         text = "Today is Sunday";
//         break;
//     default:
//         text = "Looking forward to the Weekend";
// }

// console.log(text)

// Loops

// while (i < 10) {
//     console.log(i)
//     i++ // i = i + 1
// }

// for (var i = 0; i < 5; i++) {
//     console.log(i)
// }

// var person = { fname: "John", lname: "Doe", age: 25 };
// (key : value)

// for (var x in person) {
//     console.log(x)
// }

// var cars = ["BMW", "Volvo", "Mini"];
// for (var x of cars) {
//     console.log(x)
// }

// tham tri
// var message1 = "hello"
// var phrase = message1
// console.log("in lan 1",phrase)
// message1 = "hi"
// console.log("in lan 2",phrase)

// tham chieu
// var user = {
//     name: "kitty"
// }
// var admin = user
// console.log(admin) => { name: 'kitty' }

// user.name = "lion"
// console.log(user) => { name: 'lion' }

// console.log(admin) 

// solve


// var admin = {}

// for (key in user) {
//     admin[key] = user[key]
// }

// console.log("in lan 1", admin)

// user.age = 20
// console.log("in lan 2", admin)

// var user = {
//     name: "kitty",
//     age: 18
// }

// var admin = Object.assign({}, user)
// console.log("in lan 1", admin)

// user.age = 20
// console.log("in lan 2", admin)

var user = {
    name: "kitty",
    age: 18,
    hobby: {
        sport: "football"
    }
}

var admin = JSON.parse(JSON.stringify(user))

console.log("in lan 1", admin)

user.hobby.sport = "soccer"
console.log("in lan 2", admin)

