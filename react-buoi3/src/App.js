import StateDemo from "./demo/StateDemo";
import EffectDemo from "./demo/EffectDemo";
import MemoDemo from "./demo/MemoDemo";
import "./App.css";

function App() {
  return (
    <div className="App">
      {/* <StateDemo /> */}
      {/* <EffectDemo /> */}
      <MemoDemo />
    </div>
  );
}

export default App;
