import { useState } from "react";

export default function StateDemo() {
  const [person, setPerson] = useState({
    title: "Thông tin cá nhân",
    name: "Lê Trung Thực",
    age: 22,
  });

  const onIncreaseAge = () => {
    setPerson({ ...person, age: person.age + 1 });
  };

  return (
    <div>
      <h1>Title: {person.title}</h1>
      <h2>Name: {person.name}</h2>
      <h2>Age: {person.age}</h2>
      <button onClick={onIncreaseAge}>Increase age</button>
    </div>
  );
}
