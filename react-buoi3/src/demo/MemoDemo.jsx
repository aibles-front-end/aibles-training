import React, { useState, useMemo } from "react";

const listName = ["Long", "Lan", "Thực", "Nam", "Hiền", "Trang", "Đại"];

export default function MemoDemo() {
  const [inputValue, setInputValue] = useState("");
  const [searchText, setSearchText] = useState(null);

  const onClickSearch = () => {
    setSearchText(inputValue);
  };

  // nếu nhập Thực lần đầu tiên, lần 2 cũng nhập Thực thì hàm sẽ ko
  // thực thi nữa vì searchText ko thay đổi
  const resultSearchedByName = useMemo(() => {
    const result = listName.find((e) => e.includes(searchText));
    console.log("🚀 result", result);
    return result;
  }, [searchText]);

  // const resultSearchedByName = () => {
  //   const result = listName.find((e) => e.includes(searchText));
  //   console.log("🚀 result", result);
  //   return result;
  // };

  return (
    <div>
      <h2>Search by name</h2>
      <input
        type="text"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />
      <button onClick={onClickSearch}>Search</button>
      <p>Kết quả : {resultSearchedByName}</p>
    </div>
  );
}
