import { useEffect, useState } from "react";

export default function EffectDemo() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    // Only execute after the first RENDER or count state changes
    console.log("useEffect has been called!");

    return () => {
      // Execute before the next effect or unmount.
      console.log("useEffect cleanup");
    };
  }, [count]);

  const onCount = () => {
    setCount(count + 1);
  };

  return (
    <div>
      <h2>Bạn đã click {count} lần</h2>
      <button onClick={onCount}>Click</button>
    </div>
  );
}
